# payload-gitlab-cms

Payload for the [GitLab CI for CMS lesson](https://awesome-workshop.github.io/gitlab-cms/).

## Accessing CVMFS

For your job to be executed in a shared runner where CVMFS is mounted and configured you just need to add to your job the tag `cvmfs`.

[.gitlab-ci.yml](.gitlab-ci.yml)